# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
![](https://i.imgur.com/uZprZvI.jpg)
    Describe how to use your web and maybe insert images to help you explain.

1. Color selector
利用input type="color"選定顏色，再將canvas的strokeStyle與fillStyle都改成該顏色。
2. Line width
利用input type="range"設定筆刷寬度，範圍是1到25，預設為1。
3. Font selector
利用select與option選擇字型，再將canvas的font改成該字型。內含三種字型: Courier New, Helvetica與Calibri，預設為Courier New。
4. Font size selector
同上，利用select與option選擇字型大小，再將canvas的fontSize改成該大小。內含六種大小: 12, 16, 20, 24, 28, 100。
5. Brush與eraser
移動滑鼠(觸發mousemove event)時，將不斷變動的游標的座標用moveTo(x1, y1)與lineTo(x2, y2)連線起來，這就是畫筆的原理。
在brush模式下時，canvas的globalCompositeOperation為source-over，較晚畫出的圖形和已存在的圖形重疊時，前者會覆蓋後者。
在eraser模式下時，globalCompositeOperation為destination-out，圖形之間有重疊時，重疊部分會被削去，這就是橡皮擦的原理。
6. Clear
使用clearRect將整個canvas清空。
7. Geometry
繪製圖形的方法是: 在滑鼠按下的同時，使用getImageData將當下的canvas狀態暫存起來。當滑鼠拖曳(觸發mousemove event)時，使用putImageData將canvas回復到滑鼠按下時的狀態，然後才繪製圖形。因為每次mousemove event都會繪製一個圖形，若沒有在每次mousemove event都回復繪製圖形前的canvas狀態，在滑鼠拖曳的同時便會一直產生圖形。
8. Text input
首先創建一個邊界與背景透明的textarea方塊，放在畫面中看不到的地方，然後，在canvas中加入click event，點擊canvas時將textarea方塊移動到游標所在位置，輸入結束後，再次點擊canvas時，使用fillText在原處繪製文字，接著再將textarea方塊移出canvas。
9. Undo and redo
利用兩個陣列(canvasHistory與redoList)，每次滑鼠放開時，將當下的canvas狀態用getImageData記錄進canvasHistory中。按下undo時，將canvas還原成canvasHistory最新的紀錄，並且將當下的canvas狀態記錄進redoList中，按下redo時，就會將redoList最新的紀錄還原到canvas中。另外，每次放開滑鼠都會將redoList清空。
10. Download
先用toDataURL將canvas取得一張當前canvas的png圖片的url，再將html的<a>元素的href屬性設定成該url，便可下載。
11. Upload
先用input type="file"選定本機端的一張圖片後，再用FileReader讀取該檔案，接著再用drawImage在canvas上繪製該圖片。




### Function description

    Decribe your bouns function and how to use it.

音樂功能: 播放愉悅的音樂，激發使用者的創作靈感。
### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

https://108062326.gitlab.io/AS_01_WebCanvas
### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
