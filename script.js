let mode = "brush";
let isDrawing = false;
let isTyping = false;
let isDrawingShapes = false;
let savedImageData;
let x = 0;
let y = 0;

const myCanvas = document.getElementById('myCanvas');
const context = myCanvas.getContext('2d');

// The x and y offset of the canvas from the edge of the page
const rect = myCanvas.getBoundingClientRect();

// Add the event listeners for mousedown, mousemove, and mouseup
myCanvas.addEventListener('mousedown', function (e) {
    x = e.clientX - rect.left;
    y = e.clientY - rect.top;
    isDrawing = true;
    if (mode == "rect" || mode == "triangle" || mode == "ellipse") {
        savedImageData = context.getImageData(0, 0, myCanvas.width, myCanvas.height);
    }
})

myCanvas.addEventListener('mousemove', function (e) {
    if (isDrawing == true) {
        if (mode == "brush" || mode == "eraser") {
            drawLine(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
            x = e.clientX - rect.left;
            y = e.clientY - rect.top;
            //can be substituted by offset.x and offset.y, both of which are experimental and not suggested to be used.
        }
        else {
            var x1 = (x <= e.clientX - rect.top) ? x : e.clientX - rect.left;
            var x2 = (x > e.clientX - rect.top) ? x : e.clientX - rect.left;
            var y1 = (y <= e.clientY - rect.top) ? y : e.clientY - rect.top;
            var y2 = (y > e.clientY - rect.top) ? y : e.clientY - rect.top;
            context.putImageData(savedImageData, 0, 0);
            if (mode == "rect") {
                context.strokeRect(x1, y1, x2 - x1, y2 - y1);
            }
            else if (mode == "ellipse") {
                var majorAxisRadius = Math.floor((x2 - x1) / 2);
                var minorAxisRadius = Math.floor((y2 - y1) / 2);
                var centerX = Math.floor((x2 + x1) / 2);
                var centerY = Math.floor((y2 + y1) / 2);
                context.beginPath();
                context.ellipse(centerX, centerY, majorAxisRadius, minorAxisRadius, 0, 0, Math.PI * 2);
                context.closePath();
                context.stroke();
            }
            else if (mode == "triangle") {
                var x3 = Math.floor((x1 + x2) / 2);
                context.beginPath();
                context.moveTo(x1, y1);
                context.lineTo(x3, y2);
                context.lineTo(x2, y1);
                context.closePath();
                context.stroke();
            }
        }
    }
})

//Note that this event "mouseup" should be monitored by the entire window element instead of only the canvas.
//Otherwise, releasing the mouse button outside the canvas will not set the "isDrawing" back to false.
//這個放開滑鼠的事件需要由整個視窗，而不只是畫布來監看，不然，將鼠標移出畫布之後再放開，並不會將isDrawing調回false
window.addEventListener('mouseup', function (e) {
    if (isDrawing == true && (mode == "brush" || mode == "eraser")) {
        drawLine(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
        x = 0;
        y = 0;
    }
    isDrawing = false;
})

//Input texts
var textbox = document.getElementById("textbox");
//Default font
context.font = "20px courier";
myCanvas.addEventListener("click", function (e) {
    if (mode == "text") {
        if (isTyping == false) {
            textX = e.clientX;
            textY = e.clientY;
            // console.log(isTyping + ',' + textX + ',' + textY);
            textbox.style.border = "1px solid black";
            textbox.style.left = e.clientX + "px";
            textbox.style.top = e.clientY + "px";
            isTyping = true;
        }
        else {
            // console.log(isTyping + ',' + textX + ',' + textY);
            isTyping = false;
            context.beginPath();
            context.fillText(textbox.value, textX, textY);   //slightly adjust its position
            context.closePath();
            textbox.style.border = "transparent";
            textbox.value = "";
            textbox.style.left = 0 + "px";
            textbox.style.top = 370 + "px";
        }
    }
})

//Defaults
context.lineWidth = 1;
context.strokeStyle = "red";
//The functions used to draw require offset coordinates
function drawLine(context, x1, y1, x2, y2) {
    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
}

//This button clears the canvas when clicked
var clearBtn = document.getElementById("clearbtn");
clearbtn.addEventListener("click", function clear() {
    savedImageData = context.getImageData(0, 0, myCanvas.width, myCanvas.height);
    context.clearRect(0, 0, myCanvas.width, myCanvas.height);
    canvasHistory = [];
    alert("Cleared!");
})

//Switch to brush mode
var brushBtn = document.getElementById("brush");
brushBtn.addEventListener("click", function brushMode() {
    mode = "brush";
    context.globalCompositeOperation = "source-over";
    context.strokeStyle = colorSelector.value;
    myCanvas.style.cursor = "url('cursor_icons/pencil.png'), auto";
})

//Switch to eraser mode
var eraserBtn = document.getElementById("eraser");
eraserBtn.addEventListener("click", function eraserMode() {
    mode = "eraser";
    myCanvas.style.cursor = "url('cursor_icons/eraser.png'), auto";
    context.globalCompositeOperation = "destination-out";
    context.strokeStyle = "rgba(255,255,255,1)";
})

var textBtn = document.getElementById("text");
textBtn.addEventListener("click", function drawText() {
    mode = "text";
    myCanvas.style.cursor = "url('cursor_icons/text.png'), auto";
    isTyping = false;
    context.font = fontSizeSelector.value + "px " + fontSelector.value;
})

var fontSelector = document.getElementById("font");
fontSelector.addEventListener("change", function changeFont() {
    context.font = fontSizeSelector.value + "px " + this.value;
    textbox.style['font-family'] = this.value;
})

var fontSizeSelector = document.getElementById("fontSize");
fontSizeSelector.addEventListener("change", function changeFontSize() {
    context.font = this.value + "px " + fontSelector.value;
    textbox.style.fontSize = this.value + "px";
})

//The scroll bar to change the line width of the stroke
var lineWidthScrollBar = document.getElementById("lineWidth");
lineWidthScrollBar.addEventListener("change", function changeLineWidth() {
    context.lineWidth = this.value;
})

//The color selector
var colorSelector = document.getElementById("colorSelector");
colorSelector.addEventListener("change", function changeColor() {
    context.strokeStyle = this.value;
    context.fillStyle = this.value;
})

var rectBtn = document.getElementById("rect");
rectBtn.addEventListener("click", function drawRect() {
    mode = "rect";
    context.strokeStyle = colorSelector.value;
    myCanvas.style.cursor = "url('cursor_icons/rect.png'), auto";
    context.globalCompositeOperation = "source-over";
})

var triangleBtn = document.getElementById("triangle");
triangleBtn.addEventListener("click", function drawTriangle() {
    mode = "triangle";
    context.strokeStyle = colorSelector.value;
    myCanvas.style.cursor = "url('cursor_icons/triangle.png'),auto";
    context.globalCompositeOperation = "source-over";
})

var ellipseBtn = document.getElementById("ellipse");
ellipseBtn.addEventListener("click", function drawEllipse() {
    mode = "ellipse";
    context.strokeStyle = colorSelector.value;
    myCanvas.style.cursor = "url('cursor_icons/circle.png'),auto";
    context.globalCompositeOperation = "source-over";
})

//Redo feature
var canvasHistory = [];
var redoList = [];
var undoBtn = document.getElementById("undo");
undoBtn.addEventListener("click", function redo() {
    // console.log(imageHistory.length);
    if (canvasHistory.length >= 2) {
        redoList.push(context.getImageData(0, 0, myCanvas.width, myCanvas.height));
        context.putImageData(canvasHistory[canvasHistory.length - 2], 0, 0);
        canvasHistory.pop();
    }
    else if (canvasHistory.length == 1) {
        redoList.push(context.getImageData(0, 0, myCanvas.width, myCanvas.height));
        context.clearRect(0, 0, myCanvas.width, myCanvas.height);
        canvasHistory.pop();
    }
})
myCanvas.addEventListener("mouseup", function recordImage() {
    canvasHistory.push(context.getImageData(0, 0, myCanvas.width, myCanvas.height));
    console.log(canvasHistory.length);
    redoList = [];
})

var redoBtn = document.getElementById("redo");
redoBtn.addEventListener("click", function redo() {
    context.putImageData(redoList[redoList.length - 1], 0, 0);
    canvasHistory.push(redoList[redoList.length - 1]);
    redoList.pop();
})

var downloadBtn = document.getElementById("download");
downloadBtn.addEventListener("click", function download() {
    var a = document.getElementById("downloader");
    a.href = myCanvas.toDataURL("image/png");
    a.click();
})

var uploader = document.getElementById("uploader");
uploader.addEventListener("change", function readImage(e) {
    var FR = new FileReader();
    FR.onload = function (e) {
        var img = new Image();
        img.src = e.target.result;
        img.onload = function () {
            context.drawImage(img, 0, 0);
        }
    }
    FR.readAsDataURL(this.files[0]);
})

var uploadBtn = document.getElementById("upload");
uploadBtn.addEventListener("click", function upload() {
    uploader.click();
})

var musicBtn = document.getElementById("music");
var isPlaying = false;
var song = new Audio("images/HEYYEYAAEYAAAEYAEYAA.mp3");
musicBtn.addEventListener("click", function play() {
    if (isPlaying == false) {
        song.play();
        isPlaying = true;
    }
    else {
        song.pause();
        isPlaying = false;
    }
})
